import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const Foto = styled.img`
width: 300px;
height: 300px;
`

const Imagen = styled.div`

background-color:#DCDCDC;
display:inline-block;
width:40%;
margin:20px

`
const Texto = styled.div`

background-color:#DCDCDC;
display:inline-block;
width:40%
margin:20px

`
const Orden_cita = styled.div`

text-align:center;
background-color:#DCDCDC;
margin:100px;

`

function App() {

  const url = "http://localhost:3003"
  const [citas, setCitas] = useState([]);

  const cargaDatos = () => {
    fetch(url + '/cita')
      .then(data => data.json())
      .then(datos => setCitas(datos))
      .catch(err => console.log(err));
  }

  useEffect(() => {
    cargaDatos();
  }, []);



  return (
    <>
    
      <Orden_cita>
        <Texto><h1>{citas.Cita}</h1></Texto>
        <Imagen><Foto src={citas.Imagen} alt="Imagen del autor" /></Imagen>
      </Orden_cita>
      
    </>
  );

}

export default App;
